package com.sda.carsharing.carsharing.controller;

import com.sda.carsharing.carsharing.model.Library;
import com.sda.carsharing.carsharing.service.LibraryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class LibraryController {

    private LibraryService libraryService;

    @Autowired
    public LibraryController(LibraryService libraryService) {
        this.libraryService = libraryService;
    }

    @GetMapping("/libs")
    public List<Library> getAllLibs(){
        return libraryService.findAll();
    }

    @GetMapping("/libs")
    public ResponseEntity<List<Library>> getAllLibsSecond(){
        return new ResponseEntity<>(libraryService.findAll(),
                HttpStatus.OK);
    }

}
