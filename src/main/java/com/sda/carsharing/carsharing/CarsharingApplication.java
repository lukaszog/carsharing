package com.sda.carsharing.carsharing;

import com.sda.carsharing.carsharing.model.Library;
import com.sda.carsharing.carsharing.repository.LibraryRepository;
import com.sda.carsharing.carsharing.service.LibraryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CarsharingApplication implements CommandLineRunner {

    @Autowired
    private LibraryService libraryService;

    public static void main(String[] args) {
        SpringApplication.run(CarsharingApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

        Library library = new Library();
        library.setName("TO JEST NAZWA");

        Library savedLib =  libraryService.addLib(library);
        System.out.println(savedLib.getId());
    }
}
