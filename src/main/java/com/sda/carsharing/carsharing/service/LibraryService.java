package com.sda.carsharing.carsharing.service;

import com.sda.carsharing.carsharing.model.Library;
import com.sda.carsharing.carsharing.repository.LibraryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LibraryService {

    @Autowired
    private LibraryRepository libraryRepository;


    public Library addLib(Library library){
        // sprawdzanie poprawnosci
        return libraryRepository.save(library);
    }

    public List<Library> findAll(){
        return libraryRepository.findAll();
    }

}
