package com.sda.carsharing.carsharing.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Library {
    @Id
    @GeneratedValue
    private long id;
    @Column
    private String name;
//    @OneToOne
//    @JoinColumn(name = "address_id")
//    @RestResource(path = "libraryAddress", rel="address")
//    private Address address;

    // standard constructor, getters, setters


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
