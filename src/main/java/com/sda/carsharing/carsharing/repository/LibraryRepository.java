package com.sda.carsharing.carsharing.repository;

import com.sda.carsharing.carsharing.model.Library;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LibraryRepository extends JpaRepository<Library, Long> {
}
